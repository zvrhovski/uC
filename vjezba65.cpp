/*
 Tema: LCD displej
 Autor: Zoran Vrhovski
 Licenca: Ovo djelo je dano na kori�tenje pod licencom Creative Commons 
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
 Vje�ba 6.5
 */ 

#include "AVR/avrlib.h"
#include "LCD/lcd.h"

#define TIPKALO1 D4
#define TIPKALO2 D2
#define BROJ_IZBORNIKA 5

void init() {
	lcdInit(); // inicijalizacija LCD displeja
	// tipkalo T1
	pinMode(TIPKALO1, INPUT_PULLUP); // PD4 konfiguriran kao ulaz
	// uklju?en pull up otpornik na pinu PD4
	// tipkalo T2
	pinMode(TIPKALO2, INPUT_PULLUP); // PD2 konfiguriran kao ulaz
	// uklju?en pull up otpornik na pinu PD2
}

int main(void) {
	
	init(); // inicijalizacija mikroupravljaca
	
	uint8_t izbornik = 1;
	uint8_t izbornikOld = 1;	
	// varijable za brojanje broj pritisaka tipkala
	uint16_t brojacTipkalo1 = 0;
	uint16_t brojacTipkalo2 = 0;
	// varijable za stare vrijednosti broja pritisaka tipkala
	uint16_t brojacTipkalo1Old = 0;
	uint16_t brojacTipkalo2Old = 0;
	
	bool ispisNaLcd = true;
	// ispis fiksnog teksta na LCD
	lcdClrScr(); 
	lcdprintf("T1: \n");
	lcdprintf("T2: ");
	
	while (1) { // beskonacna petlja
		// ako se pojavio padaju?i brid na tipkalu T2
		if (isFallingEdge(TIPKALO2)) {
			izbornik++; // pove?aj brojac za 1
			if (izbornik > BROJ_IZBORNIKA) {
				izbornik = BROJ_IZBORNIKA;
			}
		}
		// ako se pojavio padaju?i brid na tipkalu T1
		if (isFallingEdge(TIPKALO1)) {
			izbornik--; // pove?aj brojac za 1
			if (izbornik < 1) {
				izbornik = 1;
			}
		}
		// ako je bilo promjene broja?a za tipkalo T1
		if (izbornik != izbornikOld) {
			ispisNaLcd = true; // omogu?i ispis na LCD
		}		
		
		// ako je omogu?en ispis
		if (ispisNaLcd) { 
			lcdClrScr(); // brisanje znakova LCD displeja +  home pozicija kursora
			switch (izbornik) {
				case 1: 
					lcdprintf("Izbornik 1"); 					
					break;
				case 2:
					lcdprintf("Izbornik 2");
					break;
				case 3:
					lcdprintf("Izbornik 3");
					break;
				case 4:
					lcdprintf("Izbornik 4");
					break;
				case 5:
					lcdprintf("Izbornik 5");
					break;
				default:
				
					break;
			}
			ispisNaLcd = false;	// onemogu?i ispis na LCD
		}
		// na kraju while petlje pripremmiti stare vrijednosti za novi ciklus
		izbornikOld = izbornik;
	}
	return 0;
}